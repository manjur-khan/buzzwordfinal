import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordFramework
 * Created by manjur on 11/18/16 at 7:27 PM.
 */
public class GameComponents {
    FlowPane                    letterHouse                 ;   // Where all the letters are
    HBox[]                      letterBoxs                   ;   // List of all the letters house
    Label[]                     letters                     ;   // List of all the letters in labels
    private BorderPane          root                        ;   // Instance of root
    Controller                  controller                  ;   // reference to controller

    public GameComponents(BorderPane root, Label[] letters) {
        this.root = root;
        this.letters = letters;
    }

    public GameComponents(BorderPane root) {
        this.root = root;
    }

    public GameComponents(BorderPane root, Controller controller) {
        this.root = root;
        this.controller = controller;
    }

    public void setLetters(Label[] letters) {
        this.letters = letters;
        makeHBoxes();
    }

    void setController(Controller controller) {
        this.controller = controller;
    }

    public void makeHomeScreen() {
        defaultLabels();
        makeHBoxes();
        unifyFlowPane(new FlowPane(letterBoxs));
        root.setCenter(letterHouse);
    }

    public void remakeCenter() {
        unifyFlowPane(new FlowPane(letterBoxs));
        root.setCenter(letterHouse);
    }

    private void defaultLabels() {
        letters = new Label[16];
        char[] chars = {'B', 'U', ' ', ' ', 'Z', 'Z', ' ', ' ',
                ' ', ' ', 'W', 'O', ' ', ' ', 'R', 'D'};
        for(int i = 0; i < chars.length; i++) {
            letters[i] = GameComponents.unifyLabel(chars[i]);
        }
    }

    private void unifyFlowPane(FlowPane flowPane) {
        flowPane.setHgap(15);
        flowPane.setVgap(15);
        flowPane.setAlignment(Pos.CENTER);
        flowPane.setMaxSize(400, 400);
        flowPane.setMinSize(400, 400);
        this.letterHouse = flowPane;
    }

    public void makeHBoxes() {
        letterBoxs = new HBox[letters.length];
        for(int i = 0; i < letters.length; i++) {
            letterBoxs[i] = unifyHbox(letters[i]);
            letterBoxs[i].setId("" + i);
        }
    }

    private HBox unifyHbox(Label label) {
        HBox temp = new HBox(label);
        //temp.setId(label.getId());
        temp.setDisable(label.isDisable());
        temp.setMaxSize(70, 70);
        temp.setMinSize(70, 70);
        temp.setStyle("-fx-background-radius: 50%;" +
                "-fx-background-color: linear-gradient(to bottom, derive(cadetblue, 20%), cadetblue);");
        temp.setEffect(new DropShadow(10, 0, 4, Color.BLACK));
        return temp;
    }

    public static Label unifyLabel(char character) {
        Label label = new Label(Character.toString(character));
        label.setId(Character.toString(character));
        label.setFont(Font.font("Verdana", 40));
        label.setAlignment(Pos.CENTER);
        label.setMaxSize(70, 70);
        label.setMinSize(70, 70);
        return label;
    }

    public static Label unifyLabel(char character, boolean disable) {
        Label label = new Label(Character.toString(character));
        label.setId(Character.toString(character));
        label.setFont(Font.font("Verdana", 40));
        label.setAlignment(Pos.CENTER);
        label.setDisable(!disable);
        label.setMaxSize(70, 70);
        label.setMinSize(70, 70);
        return label;
    }

    public void makeGameLevels(int i) {
        letters = new Label[i];
        for(int k = 0; k < letters.length; k++) {
            letters[i] = GameComponents.unifyLabel(Integer.toString(i).charAt(0));
        }
        makeHBoxes();
        remakeCenter();
    }
    void isLetterBoxHighlighted(double x, double y) {
        for (int i = 0; i < letterBoxs.length; i++) {
            if(letterBoxs[i].isHover()) {
                if (!controller.boardLogic.getCoordinate(i, controller.boardLogic.row, controller.boardLogic.col)) {
                    resetColor();
                    controller.boardLogic.que = new ArrayList<>();
                }
            }
        }
    }

    void gameReady() {
        /*root.getCenter().setOnMousePressed((event -> {
            while(root.getCenter().isPressed()) {
                isLetterBoxHighlighted(event.getX(), event.getY());
            }
        }));*/

        root.setOnKeyPressed((event -> {
            try {
                char ch = (char)(event.getText().charAt(0) - 56);
                if (controller.boardLogic.getID(ch) >= 0) {
                    if (!controller.boardLogic.getCoordinate(controller.boardLogic.getID(ch), controller.boardLogic.row, controller.boardLogic.col)) {
                        resetColor();
                        controller.boardLogic.que = new ArrayList<>();
                    }
                    controller.rightSide.highLight(ch - 'A');
                }
                remakeCenter();
                System.out.println(event.getText().charAt(0));
            }catch (Exception e) {
                e.printStackTrace();
                Popup.showError("Illegal key pressed");
            }
        }));



        for (int i = 0; i < letterBoxs.length; i++) {
            HBox temp = letterBoxs[i];



            temp.setOnMouseClicked((event -> {
                if (!controller.boardLogic.getCoordinate(new Integer(temp.getId()), controller.boardLogic.row, controller.boardLogic.col)) {
                    resetColor();
                    controller.boardLogic.que = new ArrayList<>();
                }
                controller.rightSide.highLight(letters[new Integer(temp.getId())].getId().charAt(0) - 'A');
            }));

            //letterHouse.setOnMouseReleased();
            controller.rightSide.wordsGuessed.setOnMouseClicked((event -> {
                controller.boardLogic.checkQue();
                resetColor();
            }));

            letterBoxs[i] = temp;

        }
        remakeCenter();
    }

    void resetColor() {
        for(int i = 0; i < letterBoxs.length; i++) {
            letterBoxs[i].setStyle("-fx-background-radius: 50%;" +
                    "-fx-background-color: linear-gradient(to bottom, derive(cadetblue, 20%), cadetblue);");
        }
        remakeCenter();
    }
}