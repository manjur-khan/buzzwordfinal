import java.util.ArrayList;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordProject
 * Created by manjur on 11/28/16 at 8:32 PM.
 */
public class Level {
    boolean             completed               ;   // level completed
    int                 highScore               ;   // level high score
    ArrayList<Integer>  scores                  ;   // highest score on level
    int                 levelValue              ;   // value of level
    boolean             unlocked                ;   // has the level been unlocked yet

    public Level(int levelValue) {
        this.levelValue = levelValue;
        completed = false;
        highScore = 0;
        if(levelValue == 1) {
            unlocked = true;
        } else {
            unlocked = false;
        }
        scores = new ArrayList<>();
        scores.add(0);
    }

    public Level(int levelValue, boolean completed, boolean unlocked, int highScore, ArrayList<Integer> scores) {
        this.unlocked = unlocked;
        this.levelValue = levelValue;
        this.completed = completed;
        this.highScore = highScore;
        this.scores = scores;
    }

    public int getHighScore() {
        return highScore;
    }

    public void addNewScore(int i) {
        if (highScore < i) {
            highScore = i;
        }
        scores.add(i);
    }

    public int getLevelValue() {
        return levelValue;
    }

    public int getScore() {
        return scores.get(scores.size()-1);
    }

    @Override
    public String toString() {
        String toReturn = "*****\n";            // 5 star means its a new level
        toReturn += (Integer.toString(levelValue) + "\n"); // value of level
        toReturn += (Boolean.toString(completed) + "\n"); // is completed
        toReturn += (Boolean.toString(unlocked) + "\n"); // is unlocked
        toReturn += (Integer.toString(highScore) + "\n"); // Highest score in level
        String temp = "";
        for(int i = 0; i < scores.size(); i++) {  // list of all the scores
            temp += (Integer.toString(scores.get(i)) + " ");
        }
        toReturn += (temp + "\n"); // list of all the scores

        return toReturn;
    }
}
