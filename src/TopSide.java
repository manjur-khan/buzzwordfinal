import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Font;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordFramework
 * Created by manjur on 11/18/16 at 6:36 PM.
 */
public class TopSide {
    private BorderPane          root                ;   // The instance of the original root
    Label               topHeadder          ;   // The heading on the page
    FlowPane            topFlowPane         ;   // The top header house
    private Controller          controller          ;   // universal controller

    public TopSide(BorderPane root) {
        this.root = root;
        makeHomePage();
    }

    public void makeHomePage() {
        topHeadder = unifyLabel(new Label("¡¡BuzzWord!!"));
        topHeadder.setFont(Font.font("Verdana", 40));
        topFlowPane = new FlowPane(topHeadder);
        topFlowPane.setAlignment(Pos.CENTER);
        topFlowPane.setStyle("-fx-background-color: lightgray;");
        topFlowPane.setMaxSize(1000, 100);
        topFlowPane.setMinSize(1000, 100);
        root.setTop(topFlowPane);
    }
    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void makeTypeSelectionPage() {
        topHeadder = unifyLabel(new Label("Select Mode"));
        topHeadder.setFont(Font.font("Verdana", 30));
        topFlowPane = new FlowPane(topHeadder);
        topFlowPane.setAlignment(Pos.CENTER);
        topFlowPane.setStyle("-fx-background-color: lightgray;");
        topFlowPane.setMaxSize(1000, 100);
        topFlowPane.setMinSize(1000, 100);
        root.setTop(topFlowPane);
    }

    public void makeLevelSelectionPage() {
        topHeadder = unifyLabel(new Label(""));
        topHeadder.setFont(Font.font("Verdana", 30));
        topFlowPane = new FlowPane(topHeadder);
        topFlowPane.setAlignment(Pos.CENTER);
        topFlowPane.setStyle("-fx-background-color: lightgray;");
        topFlowPane.setMaxSize(1000, 100);
        topFlowPane.setMinSize(1000, 100);
        root.setTop(topFlowPane);
    }

    private Label unifyLabel(Label text) {
        text.setFont(Font.font("Verdana", 30));
        text.setAlignment(Pos.CENTER);
        text.setMouseTransparent(true);

        return text;
    }

}