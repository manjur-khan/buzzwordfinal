import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordProject
 * Created by manjur on 11/28/16 at 8:28 PM.
 */
public class User {
    GamePart[]                gamePart                ;   // different games
    String[]                  userInfo                ;   // userInfo

    public User() {
        gamePart = new GamePart[4];
        gamePart[0] = new GamePart("Animals", "animalnames.txt");
        gamePart[1] = new GamePart("Plants", "plantnames.txt");
        gamePart[2] = new GamePart("Places", "placesnames.txt");
        gamePart[3] = new GamePart("Games", "gamesnames.txt");
    }

    public void userInfoUpdate(String[] userInfo) {
        this.userInfo = userInfo;
        buildUserInfo();
    }

    public int gamepartFinder(String str) {
        if("Animals".contains(str)) {
            return 0;
        } else if("Plants".contains(str)) {
            return 1;
        } else if("Places".contains(str)) {
            return 2;
        } else if("Games".contains(str)) {
            return 3;
        }
        return -1;
    }

    public GamePart getGamePart(int i) {
        return gamePart[i];
    }

    void buildUserInfo() {
        try {
            File file = new File(userInfo[3]);
            try {
                Scanner reader = new Scanner(file);
                while (reader.hasNextLine()) {
                    String temp = reader.nextLine();
                    if (temp.equals("**********")) {
                        temp = reader.nextLine();
                        switch (temp) {
                            case "Animals":
                                temp = reader.nextLine();
                                gamePart[0].setMaxLevelsCompleted(Integer.parseInt(temp));
                                for (int k = 0; k < gamePart[0].getMaxLevels(); k++) {
                                    temp = reader.nextLine();
                                    if (temp.equals("*****")) {
                                        temp = reader.nextLine();
                                        int levelValue = Integer.parseInt(temp);
                                        temp = reader.nextLine();
                                        boolean completed = Boolean.parseBoolean(temp);
                                        temp = reader.nextLine();
                                        boolean unlocked = Boolean.parseBoolean(temp);
                                        temp = reader.nextLine();
                                        int highScore = Integer.parseInt(temp);
                                        temp = reader.nextLine();
                                        ArrayList<Integer> arrayList = new ArrayList<>();
                                        String[] array = temp.split(" ");
                                        for (int j = 0; j < array.length; j++) {
                                            arrayList.add(Integer.parseInt(array[j]));
                                        }
                                        Level tempLevel = new Level(levelValue, completed, unlocked, highScore, arrayList);
                                        gamePart[0].updateLevel(tempLevel, k);
                                    }
                                }
                                break;
                            case "Plants":
                                temp = reader.nextLine();
                                gamePart[1].setMaxLevelsCompleted(Integer.parseInt(temp));
                                for (int k = 0; k < gamePart[1].getMaxLevels(); k++) {
                                    temp = reader.nextLine();
                                    if (temp.equals("*****")) {
                                        temp = reader.nextLine();
                                        int levelValue = Integer.parseInt(temp);
                                        temp = reader.nextLine();
                                        boolean completed = Boolean.parseBoolean(temp);
                                        temp = reader.nextLine();
                                        boolean unlocked = Boolean.parseBoolean(temp);
                                        temp = reader.nextLine();
                                        int highScore = Integer.parseInt(temp);
                                        temp = reader.nextLine();
                                        ArrayList<Integer> arrayList = new ArrayList<>();
                                        String[] array = temp.split(" ");
                                        for (int j = 0; j < array.length; j++) {
                                            arrayList.add(Integer.parseInt(array[j]));
                                        }

                                        Level tempLevel = new Level(levelValue, completed, unlocked, highScore, arrayList);
                                        gamePart[1].updateLevel(tempLevel, k);
                                    }
                                }
                                break;
                            case "Places":
                                temp = reader.nextLine();
                                gamePart[2].setMaxLevelsCompleted(Integer.parseInt(temp));
                                for (int k = 0; k < gamePart[2].getMaxLevels(); k++) {
                                    temp = reader.nextLine();
                                    if (temp.equals("*****")) {
                                        temp = reader.nextLine();
                                        int levelValue = Integer.parseInt(temp);
                                        temp = reader.nextLine();
                                        boolean completed = Boolean.parseBoolean(temp);
                                        temp = reader.nextLine();
                                        boolean unlocked = Boolean.parseBoolean(temp);
                                        temp = reader.nextLine();
                                        int highScore = Integer.parseInt(temp);
                                        temp = reader.nextLine();
                                        ArrayList<Integer> arrayList = new ArrayList<>();
                                        String[] array = temp.split(" ");
                                        for (int j = 0; j < array.length; j++) {
                                            arrayList.add(Integer.parseInt(array[j]));
                                        }

                                        Level tempLevel = new Level(levelValue, completed, unlocked, highScore, arrayList);
                                        gamePart[2].updateLevel(tempLevel, k);
                                    }
                                }
                                break;
                            case "Games":
                                temp = reader.nextLine();
                                gamePart[3].setMaxLevelsCompleted(Integer.parseInt(temp));
                                for (int k = 0; k < gamePart[3].getMaxLevels(); k++) {
                                    temp = reader.nextLine();
                                    if (temp.equals("*****")) {
                                        temp = reader.nextLine();
                                        int levelValue = Integer.parseInt(temp);
                                        temp = reader.nextLine();
                                        boolean completed = Boolean.parseBoolean(temp);
                                        temp = reader.nextLine();
                                        boolean unlocked = Boolean.parseBoolean(temp);
                                        temp = reader.nextLine();
                                        int highScore = Integer.parseInt(temp);
                                        temp = reader.nextLine();
                                        ArrayList<Integer> arrayList = new ArrayList<>();
                                        String[] array = temp.split(" ");
                                        for (int j = 0; j < array.length; j++) {
                                            arrayList.add(Integer.parseInt(array[j]));
                                        }

                                        Level tempLevel = new Level(levelValue, completed, unlocked, highScore, arrayList);
                                        gamePart[3].updateLevel(tempLevel, k);
                                    }
                                }
                                break;
                            default:
                        }
                    }
                }

                reader.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                //return false;
            }
        }catch (Exception e) {
            e.printStackTrace();
            //return false;
        }
        //return true;
    }

    public GamePart[] getGamePart() {
        return gamePart;
    }

    public boolean saveUserInfo() {
        try {
            PrintWriter writer = new PrintWriter(userInfo[3], "UTF-8");
            for(int i = 0; i < gamePart.length; i++) {
                writer.println(gamePart[i].toString());
            }
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}
