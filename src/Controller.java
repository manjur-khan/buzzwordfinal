import javafx.scene.control.Button;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.layout.HBox;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordProject
 * Created by manjur on 11/30/16 at 9:59 PM.
 */
public class Controller {
    RightSide                   rightSide                   ;   // reference to right side
    LeftSide                    leftSide                    ;   // reference to left side
    TopSide                     topSide                     ;   // reference to top side
    BottomSide                  bottomSide                  ;   // reference to bottom side
    CenterSide                  centerSide                  ;   // reference to center side
    User                        user                        ;   // reference to user
    GamePart[]                  gameParts                   ;   // reference to game parts
    boolean                     userLoggedIn                ;   // reference to user Loggedin
    String[]                    userInfo                    ;   // user info
    BoardLogic                  boardLogic                  ;   // how the letters in the board created
    Help                        help                        ;   // help



    public Controller(RightSide rightSide, LeftSide leftSide, TopSide topSide,
                      BottomSide bottomSide, CenterSide centerSide) {
        this.rightSide = rightSide;
        this.leftSide = leftSide;
        this.topSide = topSide;
        this.bottomSide = bottomSide;
        this.centerSide = centerSide;
        boardLogic = new BoardLogic(this);
        user = new User();
        help = new Help();
        innitializeControllers();
        makeLoginScreen();
    }

    private void innitializeControllers() {
        rightSide.setController(this);
        leftSide.setController(this);
        topSide.setController(this);
        bottomSide.setController(this);
        centerSide.setController(this);
    }

    public void makeLoginScreen() {
        leftSide.makeLoginScreen();
    }

    public void innitializeLoginScreenButton() {

        centerSide.root.setOnKeyPressed((event -> {
            if (event.isControlDown() && event.getText().charAt(0) == 'h') {
                help.show();
            }
        }));


        leftSide.helpButton.setOnAction((event -> {
            help.show();
        }));
        leftSide.loginButton.setOnAction((event) -> {
            centerSide.loginScreen();
        });

        leftSide.registerButton.setOnAction((event) -> {
            centerSide.signupScreen();
        });
    }

    public void logOut() {
        try {
            centerSide.timer.cancel();
            userLoggedIn = false;
            user.saveUserInfo();
            user = new User();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void innitializeHomeScreenButton() {

        centerSide.root.setOnKeyPressed((event -> {
            if(event.isControlDown() && event.getText().charAt(0)=='l') {
                if(userLoggedIn && !centerSide.isPlaying) {
                    leftSide.makeLoginScreen();
                    userInfo = null;
                    centerSide.makeHomePage();
                    rightSide.makeHomePage();
                    topSide.makeHomePage();
                    bottomSide.makeHomePage();
                    logOut();
                } else {
                    centerSide.loginScreen();
                }
            }
        }));
        leftSide.userButton.setOnAction((event -> {
            centerSide.displayUserInfo();
            leftSide.userButton.setDisable(true);
            leftSide.startPlaying.setDisable(true);
        }));

        leftSide.logout.setOnAction((event -> {
            leftSide.makeLoginScreen();
            userInfo = null;
            centerSide.makeHomePage();
            rightSide.makeHomePage();
            topSide.makeHomePage();
            bottomSide.makeHomePage();
            logOut();

        }));

        leftSide.startPlaying.setOnAction((event -> {
            topSide.makeTypeSelectionPage();
            leftSide.startPlaying.setVisible(false);
            leftSide.homeButton.setVisible(true);
            leftSide.userButton.setVisible(false);
            centerSide.makeGameTypeSelection();
        }));

        leftSide.homeButton.setOnAction((event -> {
            if(centerSide.isPlaying) {
                centerSide.root.getCenter().setVisible(false);
                centerSide.root.getLeft().setDisable(true);
                centerSide.timer.cancel();
                centerSide.isPlaying = !Popup.yesNoCancelBox("Are you sure you want to quit? You will lose it all");
            }
            centerSide.root.getCenter().setVisible(true);
            centerSide.root.getLeft().setDisable(false);
            leftSide.startPlaying.setVisible(false);
            leftSide.pause.setVisible(true);
            //timer.cancel();
            centerSide.doYou(100, centerSide.count);

            if(!centerSide.isPlaying) {
                leftSide.makeHomeScreen();
                centerSide.makeHomePage();
                topSide.makeHomePage();
                bottomSide.makeHomePage();
                rightSide.makeHomePage();
                try {
                    centerSide.timer.cancel();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));
    }

    public void levelSelection() {
        for (int i = 0; i < centerSide.gameComponents.letterBoxs.length; i++) {
            HBox temp = centerSide.gameComponents.letterBoxs[i];
            temp.setOnMouseClicked((event -> {
                rightSide.makeGameScreen();
                Popup.showConfirmation("Your personal high score: " + user.gamePart[user.gamepartFinder(topSide.topHeadder.
                        getText().substring(0,4))].levels[new Integer(temp.getId())].getHighScore());
                centerSide.makeGamePage(boardLogic.readFile(new Integer(temp.getId())));
                topSide.topHeadder.setText(topSide.topHeadder.getText() + " : Level " + (new Integer(temp.getId()) + 1));
            }));
            centerSide.gameComponents.letterBoxs[i] = temp;
        }
    }

}
