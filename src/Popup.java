import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.swing.*;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordProject
 * Created by manjur on 11/25/16 at 4:14 PM.
 */
public class Popup {
    private static Stage               popupStage                  ;   // popup window
    private static GridPane            loginPage                   ;   // login page
    private static Button              submitButton                 ;   // Submit button
    private static String[]            userInfo                    ;   // userInfo

    public static String[] login() {
        unifyStage("Login");

        loginPage = new GridPane();
        loginPage.setMinSize(500, 200);
        loginPage.setMaxSize(500, 200);
        loginPage.setAlignment(Pos.CENTER);
        loginPage.setHgap(10);
        loginPage.setVgap(10);
        //loginPage.setPadding(new Insets(25, 25, 25, 25));

        TextField username = new TextField();
        username.setPromptText("Username");
        username.setMaxSize(400, 50);
        username.setMinSize(400, 50);

        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        password.setMaxSize(400, 50);
        password.setMinSize(400, 50);

        submitButton = new Button("Login");
        submitButton.setMaxSize(400, 50);
        submitButton.setMinSize(400, 50);

        loginPage.add(username, 0, 0);
        loginPage.add(password, 0, 1);
        loginPage.add(submitButton, 0, 2);

        Text loginError = new Text();
        loginError.setFill(Color.FIREBRICK);
        loginPage.add(loginError, 0, 4);

        popupStage.setScene(new Scene(loginPage));
        popupStage.show();

        userInfo = new String[2];

        submitButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                userInfo[0] = username.getText().trim().toLowerCase();
                userInfo[1] = password.getText();
                if(userInfo[0].isEmpty()) {
                    loginError.setText("Please enter username");
                } else {
                    if(UserInfo.getUserIndex(userInfo[0]) == -1) {
                        loginError.setText("User does not exists!");
                    } else {
                        if (userInfo[1].isEmpty()) {
                            loginError.setText("Please enter password");
                        } else {
                            userInfo = UserInfo.getUserInfo(userInfo[0], userInfo[1]);
                            if(userInfo == null) {
                                loginError.setText("Password doesn't match");
                            } else {
                                popupStage.close();
                            }
                        }
                    }
                }
            }
        });

        return userInfo;
    }

    public static String[] signUp() {
        unifyStage("Login");

        loginPage = new GridPane();
        loginPage.setMinSize(500, 400);
        loginPage.setMaxSize(500, 400);
        loginPage.setAlignment(Pos.CENTER);
        loginPage.setHgap(10);
        loginPage.setVgap(10);
        //loginPage.setPadding(new Insets(25, 25, 25, 25));

        TextField name = new TextField();
        name.setPromptText("Name");
        name.setMaxSize(400, 50);
        name.setMinSize(400, 50);

        TextField username = new TextField();
        username.setPromptText("Username");
        username.setMaxSize(400, 50);
        username.setMinSize(400, 50);


        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        password.setMaxSize(400, 50);
        password.setMinSize(400, 50);

        PasswordField password2 = new PasswordField();
        password2.setPromptText("Confirm Password");
        password2.setMaxSize(400, 50);
        password2.setMinSize(400, 50);

        submitButton = new Button("Register");
        submitButton.setMaxSize(400, 50);
        submitButton.setMinSize(400, 50);

        loginPage.add(name, 0, 0);
        loginPage.add(username, 0, 1);
        loginPage.add(password, 0, 2);
        loginPage.add(password2, 0, 3);
        loginPage.add(submitButton, 0, 4);

        Text loginError = new Text();
        loginError.setFill(Color.FIREBRICK);
        loginPage.add(loginError, 0, 5);

        popupStage.setScene(new Scene(loginPage));
        popupStage.show();

        userInfo = new String[4];

        submitButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                userInfo[0] = username.getText().trim().toLowerCase();
                userInfo[1] = name.getText().trim();
                userInfo[2] = password.getText();
                if(userInfo[0].isEmpty()) {
                    loginError.setText("Please enter username");
                } else {
                    if(UserInfo.getUserIndex(userInfo[0]) > -1) {
                        loginError.setText("Username is already taken. Try new username!");
                    } else {
                        if (userInfo[1].isEmpty()) {
                            loginError.setText("Please enter your name.");
                        } else {
                            if(userInfo[2].isEmpty()) {
                                loginError.setText("Please enter password!");
                            } else {
                                if(!userInfo[2].equals(password2.getText())) {
                                    loginError.setText("Password doesn't match confirm password");
                                } else {
                                    userInfo[3] = userInfo[0] + ".txt";
                                    //UserInfo.updateUserInfo(userInfo);
                                    popupStage.close();
                                }
                            }
                        }
                    }
                }
            }
        });

        return userInfo;
    }

    public static void showConfirmation(String text) {
        JOptionPane.showMessageDialog(null, text, "Confirmation", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void showError(String text) {
        JOptionPane.showMessageDialog(null, text, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static boolean yesNoCancelBox(String text) {
        int n = JOptionPane.showConfirmDialog(null, text, "Are you sure?", JOptionPane.YES_NO_CANCEL_OPTION);
        return n == JOptionPane.YES_OPTION;
    }
    public static int YesNoCancelBox(String text) {
        return JOptionPane.showConfirmDialog(null, text, "Are you sure?", JOptionPane.YES_NO_CANCEL_OPTION);
    }

    public static void message(String msg) {
        unifyStage("Message");

        loginPage = new GridPane();
        loginPage.setAlignment(Pos.CENTER);
        loginPage.setHgap(10);
        loginPage.setVgap(10);

        Text text = new Text();
        text.setFont(Font.font("Helvetica", 25));
        text.setText(msg);

        loginPage.add(text, 0, 0);
        popupStage.setScene(new Scene(loginPage));
        popupStage.show();
    }


    private static void unifyStage(String title) {
        popupStage = new Stage();
        popupStage.setTitle(title);
        popupStage.alwaysOnTopProperty();
        popupStage.setWidth(600);
        popupStage.setHeight(500);
        popupStage.setResizable(false);
    }
}
