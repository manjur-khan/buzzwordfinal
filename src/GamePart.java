import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordProject
 * Created by manjur on 11/28/16 at 8:32 PM.
 */
public class GamePart {
    Level[]                 levels              ;   // levels in game
    int                     maxLevels           ;   // maximum levels
    int                     maxWords            ;   // max words
    int                     maxLevelCompleted   ;   // max level completed
    String                  fileName            ;   // File to read from
    String                  partName            ;   // name of game part
    ArrayList<String>       allWords            ;   // List of all words

    public GamePart(String name, String fileName) {
        partName = name;
        this.fileName = fileName;
        maxLevelCompleted = 0;
        allWords = new ArrayList<String>();
        File file = new File(fileName);
        try {
            Scanner reader = new Scanner(file);
            maxLevels = new Integer(reader.nextLine());
            maxWords = new Integer(reader.nextLine());

            levels = new Level[maxLevels];

            int counter = 0;
            while(reader.hasNextLine()){
                String temp = reader.nextLine();
                allWords.add(temp);
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < levels.length; i++) {
            levels[i] = new Level(i + 1);
        }
    }

    public void unlockNextLevel(int i) {
        if(i!= levels.length-1) {
            levels[i+1].unlocked = true;
        }
    }

    public void setMaxLevelsCompleted(int maxLevelCompleted) {
        this.maxLevelCompleted = maxLevelCompleted;
    }

    public int getMaxLevels() {
        return maxLevels;
    }

    public void updateLevel(Level level, int i) {
        levels[i] = level;
    }

    public int getMaxLevelCompleted() {
        return maxLevelCompleted;
    }

    @Override
    public String toString() {
        String toReturn = "**********\n";
        toReturn += (partName + "\n");
        toReturn += (Integer.toString(maxLevelCompleted) + "\n");
        for(int i = 0; i < levels.length; i++) {
            toReturn += (levels[i].toString());
        }

        return toReturn;
    }

}
