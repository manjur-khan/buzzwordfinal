import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by Manjur on 12/11/2016.
 */
public class Help {
    public Stage helpStage;
    public GridPane grids;
    public ScrollPane scrollPane;

    public Help() {
        try {
            Scanner reader = new Scanner(new File("help.txt"));
            helpStage = new Stage();
            helpStage.setTitle("Help!");
            helpStage.setWidth(500);
            helpStage.setHeight(500);

            grids = new GridPane();
            grids.setVgap(10);
            grids.setHgap(15);
            int row = 0;
            while(reader.hasNextLine()) {
                String[] str = reader.nextLine().split("====");
                Label title = new Label(str[0]);
                Text text = new Text(str[1]);
                grids.add(title, 0, row);
                grids.add(text, 1, row);

                row++;
            }
            reader.close();

            scrollPane = new ScrollPane(grids);
            scrollPane.setMinSize(400, 400);
            scrollPane.setMaxSize(400, 400);

            helpStage.setScene(new Scene(scrollPane, 500, 500));


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    void show() {
        helpStage.show();
    }

    void close() {
        helpStage.hide();
    }
}
