import javafx.scene.control.Label;

import javax.swing.*;
import java.io.CharArrayReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by Manjur on 12/11/2016.
 */
public class BoardLogic {
    private Controller      controller                      ;   // univeral controller
    public ArrayList<String> words;
    public char[][] box  = new char[4][4];
    public ArrayList<String> word = new ArrayList<>();
    public ArrayList<Integer> points = new ArrayList<>(); //points
    public ArrayList<String> goodWords; // good words
    public char[] neighbor = new char[4];
    public int row;
    public int col;
    public boolean boxComplete;
    public int totalScore;
    public int currentScore;
    public ArrayList<Character> que = new ArrayList<>();


    public char[] readFile(ArrayList<String> words, int level) {
        this.words = new ArrayList<>();
        this.words = words;
        innitializeAll(words.size(), level);
        makeGrid();
        char[] toReturn = new char[16];
        for(int i = 0; i < toReturn.length; i++) {
            toReturn[i] = box[i / 4][i % 4];
        }
        return toReturn;
    }

    public BoardLogic(Controller controller) {
        this.controller = controller;
    }

    public char[] readFile(int level) {
        innitializeAll(words.size(), level);
        makeGrid();
        char[] toReturn = new char[16];
        for(int i = 0; i < toReturn.length; i++) {
            toReturn[i] = box[i / 4][i % 4];
        }
        return toReturn;
    }

    public void setWords(ArrayList<String> words) {
        this.words = new ArrayList<>();
        this.words = words;
    }

    public void innitializeAll(int num, int level) {
        goodWords = new ArrayList<>();
        word = new ArrayList<>();
        while(word.size() < 6) {
            String temp = words.get(random(num)).toUpperCase();
            if(level < 3) {
                while (temp.length() > 4 || word.contains(temp)) {
                    temp = words.get(random(num)).toUpperCase();
                }
            } else if (level < 3) {
                while (temp.length() < 3 || word.contains(temp)) {
                    temp = words.get(random(num)).toUpperCase();
                }
            } else {
                while (temp.length() < 4 || word.contains(temp)) {
                    temp = words.get(random(num)).toUpperCase();
                }
            }
            System.out.print(temp + " ");
            word.add(temp);
        }
        System.out.println();
        for(int i = 0; i < box.length; i++) {
            for(int j = 0; j < box[i].length; j++) {
                box[i][j] = '-';
            }
        }
    }

    public void makeGrid() {
        row = random(4);
        col = random(4);
        boxComplete = false;
        for (int i = 0; i < word.size(); i++) {
            for (int j = 0; j < word.get(i).length(); j++) {
                if (box[row][col] == '-') {
                    box[row][col] = word.get(i).charAt(j);
                    neighbors(row, col);
                    if(j == word.get(i).length() - 1) {
                        goodWords.add(word.get(i));
                    }
                    if (!analyze() && j < word.get(i).length() - 1) {
                        break;
                    }
                }
            }
        }
        printgrid();
        finalWords();
    }

    void finalWords() {
        totalScore = 0;
        currentScore = 0;
        for(int i = 0; i < goodWords.size(); i++) {
            int num = 0;
            for(int j = 0; j < goodWords.get(i).length(); j++) {
                num += power(goodWords.get(i).charAt(j), 0.75);
            }
            num = power(num, 0.7);
            points.add(num);
            totalScore += num;
            System.out.print(num + " = " + goodWords.get(i) + " ");
        }
    }

    int power(double num, double pow) {
        return (int) Math.pow(num, pow);
    }


    void printgrid() {
        for(int i = 0; i < box.length; i++) {
            for(int j = 0; j < box[i].length; j++) {
                System.out.print(box[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("\n\n");
    }

    boolean checkValidEntry(int num, char newLetter) {
        row = num / 4;
        col = num % 4;
        neighbors(row, col);
        return false;
    }

    boolean getCoordinate(int num, int i, int j) {
        if(que.isEmpty()) {
            row = num / 4;
            col = num % 4;
            que.add(box[row][col]);
            controller.centerSide.gameComponents.letterBoxs[num].setStyle(
                    "-fx-background-color: #ADD8E6;"
            );
            return true;
        }

        if(((i-1)*4) + j == num || ((i)*4) + (j+1) == num ||
                ((i+1)*4) + j == num || ((i)*4) + (j-1) == num ) {
            row = num / 4;
            col = num % 4;
            que.add(box[row][col]);
            controller.centerSide.gameComponents.letterBoxs[num].setStyle(
                    "-fx-background-color: #ADD8E6;"
            );
            return true;
        }
        return false;
    }

    void checkQue() {
        String temp = "";
        for (int i = 0; i < que.size(); i++) {
            temp += que.get(i);
        }

        if(goodWords.indexOf(temp) > -1) {
            currentScore += points.get(goodWords.indexOf(temp));
            Label label = new Label(temp);
            Label label1 = new Label("" + points.get(goodWords.indexOf(temp)));
            label.setMaxSize(120, 40);
            label.setMinSize(120, 40);
            label1.setMaxSize(120, 40);
            label1.setMinSize(120, 40);

            controller.rightSide.wordsGuessed.getChildren().addAll(label, label1);
            controller.rightSide.setCurrentScore(currentScore);

            points.remove(goodWords.indexOf(temp));
            goodWords.remove(temp);
        }
        que = new ArrayList<>();

        if(currentScore >= totalScore) {
            String tempName = controller.topSide.topHeadder.getText().substring(0, 4);
            int i = controller.topSide.topHeadder.getText().charAt(controller.topSide.topHeadder.getText().length() - 1) - '1';
            controller.user.gamePart[controller.user.gamepartFinder(tempName)].levels[i].completed = true;
            controller.user.gamePart[controller.user.gamepartFinder(tempName)].levels[i].addNewScore(currentScore);
            controller.user.gamePart[controller.user.gamepartFinder(tempName)].unlockNextLevel(i);
            System.out.println(controller.user.gamePart[controller.user.gamepartFinder(tempName)].levels[i].toString());
            int answer = Popup.YesNoCancelBox("Congratulation. You successfully completed this level. \n" +
                    "Would you like to move onto the next level?");
            controller.centerSide.timer.cancel();
            if(answer == JOptionPane.YES_OPTION) {
                controller.rightSide.makeGameScreen();
                controller.centerSide.makeGamePage(readFile(new Integer(controller.topSide.topHeadder.getText().substring(
                        controller.topSide.topHeadder.getText().length() - 1)) + 1));

                controller.topSide.topHeadder.setText(controller.topSide.topHeadder.getText().substring(0, controller.topSide.topHeadder.getText().length() - 1) +
                        (new Integer(controller.topSide.topHeadder.getText().substring(controller.topSide.topHeadder.getText().length() - 1)) + 1));
            }
        }

    }

    public void keyPressed(char key) {
        controller.rightSide.highLight(key - 'A');
        if(validKey(key)) {
            que.add(box[row][col]);
            controller.centerSide.gameComponents.letterBoxs[row*4+col].setStyle(
                    "-fx-background-color: #ADD8E6;"
            );
        } else {
            controller.centerSide.gameComponents.resetColor();
        }
    }

    int getID(char key) {
        int id = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (box[i][j] == key) {
                    return id;
                }
                id++;
            }
        }
        return -1;
    }

    boolean validKey(char key) {
        if(que.isEmpty()) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (box[i][j] == key) {
                        row = i;
                        col = j;
                        neighbors(row, col);
                        return true;
                    }
                }
            }
        } else {
            for (int i = 0; i < neighbor.length; i++) {
                if(neighbor[i] == key) {
                    setCordinate(i);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean analyze() {
        if(isValid()) {
            int r = random(neighbor.length) - 1;
            do {
                r++;
                if(r == 4) {
                    r = 0;
                }
            } while(neighbor[r] != '-'/* && neighbor[r] != box[row][col]*/);
            setCordinate(r);
            return true;
        } else {
            lookFor();
        }
        return false;
    }
    public void lookFor() {
        boxComplete = true;
        for(int i = 0; i < box.length; i++) {
            for (int j = 0; j < box[i].length; j++) {
                if(box[i][j] == '-') {
                    boxComplete = false;
                    row = i;
                    col = j;
                }
            }
        }
    }

    public void setCordinate(int num) {
        switch (num) {
            case 0 :
                row--;
                break;
            case 1:
                col++;
                break;
            case 2:
                row++;
                break;
            case 3:
                col--;
                break;
        }
    }

    public boolean isValid() {
        for(int i = 0; i < neighbor.length; i++) {
            if(neighbor[i] == '-' /*|| neighbor[i] == box[row][col]*/) {
                return true;
            }
        }
        return false;
    }

    void neighbors(int i, int j) {
        if(i != 0) { // i-1 < 0 >> top
            neighbor[0] = box[i-1][j];
        } else {
            neighbor[0] = 0;
        }

        if(j != 3) { // j - 1 < 0 >> right
            neighbor[1] = box[i][j+1];
        } else {
            neighbor[1] = 0;
        }

        if(i != 3) { // i+1 > 3 >> bottom
            neighbor[2] = box[i+1][j];
        } else {
            neighbor[2] = 0;
        }

        if(j != 0) { // j-1 < 0 >> left
            neighbor[3] = box[i][j-1];
        } else {
            neighbor[3] = 0;
        }

    }

    public int random(int num) {
        return (int)(Math.random()*num);
    }

}
