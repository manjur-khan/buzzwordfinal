import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Font;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordFramework
 * Created by manjur on 11/18/16 at 6:54 PM.
 */
public class BottomSide {
    private BorderPane              root                ;   // This is the instance from the original root
    private FlowPane                bottomFlowPane      ;   // This is the bottom housing
    private Controller              controller          ;   // universal controller
    public Button                   back                ;   // back button

    public BottomSide(BorderPane root) {
        this.root = root;
        makeHomePage();
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void makeHomePage() {
        bottomFlowPane = new FlowPane();
        bottomFlowPane.setMaxSize(1000, 100);
        bottomFlowPane.setMinSize(1000, 100);
        bottomFlowPane.setStyle("-fx-background-color: lightgray;");

        root.setBottom(bottomFlowPane);
    }

    public void makeLevelScreen() {
        back = new Button("Back");
        back.setMaxSize(150, 40);
        back.setMinSize(150, 40);
        back.setTranslateX(370);
        back.setTranslateY(30);
        back.setFont(Font.font("Verdana", 14));

        bottomFlowPane = new FlowPane();
        bottomFlowPane.setMaxSize(1000, 100);
        bottomFlowPane.setMinSize(1000, 100);
        bottomFlowPane.setStyle("-fx-background-color: lightgray;");
        bottomFlowPane.getChildren().add(back);

        root.setBottom(bottomFlowPane);

        back.setOnAction((event -> {
            controller.centerSide.makeGameTypeSelection();
            controller.topSide.makeTypeSelectionPage();
            makeHomePage();
        }));
    }
}