import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordProject
 * Created by manjur on 11/28/16 at 4:58 PM.
 */
public class UserInfo {
    private static ArrayList<String>    userName            ;   // user names from file
    private static ArrayList<String>    name                ;   // name of user from file
    private static ArrayList<String>    passWord            ;   // password from file
    private static ArrayList<String>    url                 ;   // url from file
    private static File                 userInfoFile            ;   // userInfo file

    public UserInfo() {
        name = new ArrayList<String>();
        userName = new ArrayList<String>();
        passWord = new ArrayList<String>();
        url = new ArrayList<String>();
        getUserInfo();
    }

    public static void saveUserInfo() {
        try {
            PrintWriter writer = new PrintWriter("userlogininfo.txt", "UTF-8");
            for(int i = 0; i < userName.size(); i++) {
                writer.println(userName.get(i) + "--" + name.get(i) + "--" +
                        passWord.get(i) + "--" + url.get(i));
            }
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void getUserInfo() {
        userInfoFile = new File("userlogininfo.txt");
        try {
            if (!userInfoFile.createNewFile()) {
                Scanner read = new Scanner(userInfoFile);
                while (read.hasNextLine()) {
                    String line = read.nextLine();
                    String[] array = line.split("--");
                    userName.add(array[0]);
                    name.add(array[1]);
                    passWord.add(array[2]);
                    url.add(array[3]);
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateName(String name, String userName) {
        int temp = UserInfo.userName.indexOf(userName);
        UserInfo.name.set(temp, name);
    }

    public static void updatePassword(String password, String userName) {
        int temp = UserInfo.userName.indexOf(userName);
        UserInfo.passWord.set(temp, password);
    }

    public static boolean addUser(String[] userInfo) {
        String userName = userInfo[0];
        int temp = UserInfo.userName.indexOf(userName);

        if(temp == -1) {
            UserInfo.userName.add(userInfo[0]);
            name.add(userInfo[1]);
            passWord.add(userInfo[2]);
            url.add(userName + ".txt");
            File file = new File(userName + ".txt");
            try {
                return file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static int getUserIndex(String userName) {
        return UserInfo.userName.indexOf(userName);
    }

    public static String[] getUserInfo(String userName, String password) {
        int temp = UserInfo.userName.indexOf(userName);

        if(temp == -1) {
            return null;
        }
        if(!UserInfo.passWord.get(temp).equals(password)) {
            return null;
        }
        String[] userInfo = new String[4];
        userInfo[0] = UserInfo.userName.get(temp);
        userInfo[1] = name.get(temp);
        userInfo[2] = passWord.get(temp);
        userInfo[3] = url.get(temp);

        return userInfo;
    }

}
