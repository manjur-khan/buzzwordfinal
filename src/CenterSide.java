import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.time.Duration;
import java.util.DoubleSummaryStatistics;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordFramework
 * Created by manjur on 11/18/16 at 7:18 PM.
 */
public class CenterSide {

     BorderPane                  root                ;   // innitial instance of root
    //private FlowPane                    rightFlowPane       ;   // Housing for centerside element
    GameComponents       gameComponents      ;   // All the game components
    public Button                       submitButton        ;   // submit button
    public String[]                     userInfo            ;   // user's info
    private Controller                  controller          ;   // universal controller
    Timer timer = new java.util.Timer();
    int count = 60; //timer
    boolean isPlaying                                       ;   // is the user in the middle of a game


    public CenterSide(BorderPane root) {
        this.root = root;
        gameComponents = new GameComponents(root);
        makeHomePage();
    }
    public void setController(Controller controller) {
        this.controller = controller;
        gameComponents.setController(controller);
    }

    public void makeHomePage() {
        gameComponents.makeHomeScreen();
    }

    public void makeGameComponents(int level) {
        gameComponents.makeGameLevels(level);
    }

    public void loginScreen() {
        GridPane loginPage = new GridPane();
        loginPage.setMinSize(500, 200);
        loginPage.setMaxSize(500, 200);
        loginPage.setAlignment(Pos.CENTER);
        loginPage.setHgap(10);
        loginPage.setVgap(10);
        //loginPage.setPadding(new Insets(25, 25, 25, 25));

        TextField username = new TextField();
        username.setPromptText("Username");
        username.setMaxSize(400, 50);
        username.setMinSize(400, 50);

        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        password.setMaxSize(400, 50);
        password.setMinSize(400, 50);

        submitButton = unifyButton("Login");

        loginPage.add(username, 0, 0);
        loginPage.add(password, 0, 1);
        loginPage.add(submitButton, 0, 2);

        Text loginError = new Text();
        loginError.setFill(Color.FIREBRICK);
        loginPage.add(loginError, 0, 4);

        userInfo = new String[2];
        try {
            root.setOnKeyPressed((event -> {
                if (event.getCode() == KeyCode.ENTER) {
                    userInfo[0] = username.getText().trim().toLowerCase();
                    userInfo[1] = hidePass(password.getText());
                    controller.userLoggedIn = false;
                    controller.userInfo = UserInfo.getUserInfo(userInfo[0], userInfo[1]);
                    if (controller.userInfo != null) {
                        controller.userLoggedIn = true;
                        makeHomePage();
                        controller.leftSide.makeHomeScreen();
                        controller.user.userInfo = controller.userInfo;
                    } else {
                        loginError.setText("Username or Password does not match!");
                    }
                }
            }));
        }catch (Exception e) {
            e.printStackTrace();
            Popup.showError("Illegal key pressed");
        }

        submitButton.setOnAction((event) -> {
            userInfo[0] = username.getText().trim().toLowerCase();
            userInfo[1] = hidePass(password.getText());
            controller.userLoggedIn = false;
            controller.userInfo = UserInfo.getUserInfo(userInfo[0], userInfo[1]);
            if(controller.userInfo!= null) {
                controller.userLoggedIn = true;
                makeHomePage();
                controller.leftSide.makeHomeScreen();
                controller.user.userInfo = controller.userInfo;
                controller.user.userInfoUpdate(controller.user.userInfo);
            } else {
                loginError.setText("Username or Password does not match!");
            }

        });

        root.setCenter(loginPage);

    }

    // Sign up
    public void signupScreen() {
        GridPane signUpPage = new GridPane();
        signUpPage.setMinSize(500, 400);
        signUpPage.setMaxSize(500, 400);
        signUpPage.setAlignment(Pos.CENTER);
        signUpPage.setHgap(10);
        signUpPage.setVgap(10);
        //loginPage.setPadding(new Insets(25, 25, 25, 25));

        TextField name = new TextField();
        name.setPromptText("Name");
        name.setMaxSize(400, 50);
        name.setMinSize(400, 50);

        TextField username = new TextField();
        username.setPromptText("Username");
        username.setMaxSize(400, 50);
        username.setMinSize(400, 50);


        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        password.setMaxSize(400, 50);
        password.setMinSize(400, 50);

        PasswordField password2 = new PasswordField();
        password2.setPromptText("Confirm Password");
        password2.setMaxSize(400, 50);
        password2.setMinSize(400, 50);

        submitButton = unifyButton("Register");

        signUpPage.add(name, 0, 0);
        signUpPage.add(username, 0, 1);
        signUpPage.add(password, 0, 2);
        signUpPage.add(password2, 0, 3);
        signUpPage.add(submitButton, 0, 4);

        Text signupError = new Text();
        signupError.setFill(Color.FIREBRICK);
        signUpPage.add(signupError, 0, 5);

        userInfo = new String[4];

        try {
            root.setOnKeyPressed((event -> {
                if (event.getCode() == KeyCode.ENTER) {
                    boolean missingInfo = false;
                    boolean newUser = false;
                    if(username.getText().trim().isEmpty() || !username.getText().trim().matches("[A-Za-z0-9]+")) {
                        missingInfo = true;
                    }
                    if(name.getText().trim().isEmpty() || !name.getText().trim().matches("[A-Za-z0-9 ]+")) {
                        missingInfo = true;
                    }
                    if(!password.getText().trim().equals(password2.getText().trim()) || password.getText().trim().isEmpty() ||
                            !password.getText().trim().matches("[A-Za-z0-9]+")){
                        missingInfo = true;
                    }
                    if(!missingInfo) {
                        userInfo[0] = username.getText().trim().toLowerCase();
                        userInfo[1] = name.getText().trim();
                        userInfo[2] = hidePass(password.getText());
                        userInfo[3] = userInfo[0] + ".txt";

                        newUser = UserInfo.addUser(userInfo);
                        if(newUser) {
                            makeHomePage();
                            controller.leftSide.makeLoginScreen();
                        } else {
                            signupError.setText("User Exists!");
                        }
                    } else {
                        signupError.setText("Missing Information. Also no special characters!");
                    }
                }
            }));
        }catch (Exception e) {
            e.printStackTrace();
            Popup.showError("Illegal key pressed");
        }

        submitButton.setOnAction((event -> {
            boolean missingInfo = false;
            boolean newUser = false;
            if(username.getText().trim().isEmpty() || !username.getText().trim().matches("[A-Za-z0-9]+")) {
                missingInfo = true;
            }
            if(name.getText().trim().isEmpty() || !name.getText().trim().matches("[A-Za-z0-9 ]+")) {
                missingInfo = true;
            }
            if(!password.getText().trim().equals(password2.getText().trim()) || password.getText().trim().isEmpty() ||
                    !password.getText().trim().matches("[A-Za-z0-9]+")){
                missingInfo = true;
            }
            if(!missingInfo) {
                userInfo[0] = username.getText().trim().toLowerCase();
                userInfo[1] = name.getText().trim();
                userInfo[2] = hidePass(password.getText());
                userInfo[3] = userInfo[0] + ".txt";

                newUser = UserInfo.addUser(userInfo);
                if(newUser) {
                    makeHomePage();
                    controller.leftSide.makeLoginScreen();
                } else {
                    signupError.setText("User Exists!");
                }
            } else {
                signupError.setText("Missing Information. Also no special characters!");
            }

        }));

        root.setCenter(signUpPage);

    }

    String hidePass(String pass) {
        double password = 1;
        for(int i = 0; i < pass.length(); i++) {
            password = Math.pow(password + Math.pow(pass.charAt(i), 0.7), 1.4);
        }
        return Double.toString(password);
    }


    public void displayUserInfo() {
        GridPane displayUserInfo = new GridPane();
        displayUserInfo.setMinSize(500, 400);
        displayUserInfo.setMaxSize(500, 400);
        displayUserInfo.setAlignment(Pos.CENTER);
        displayUserInfo.setHgap(10);
        displayUserInfo.setVgap(10);

        TextField name = new TextField();
        name.setText(controller.userInfo[1]);
        name.setMaxSize(400, 50);
        name.setMinSize(400, 50);

        TextField username = new TextField();
        username.setText(controller.userInfo[0]);
        username.setDisable(true);
        username.setMaxSize(400, 50);
        username.setMinSize(400, 50);

        PasswordField oldPassword = new PasswordField();
        oldPassword.setPromptText("Current Password");
        oldPassword.setMaxSize(400, 50);
        oldPassword.setMinSize(400, 50);

        PasswordField password = new PasswordField();
        password.setPromptText("New Password");
        password.setMaxSize(400, 50);
        password.setMinSize(400, 50);

        PasswordField password2 = new PasswordField();
        password2.setPromptText("Confirm New Password");
        password2.setMaxSize(400, 50);
        password2.setMinSize(400, 50);

        submitButton = unifyButton("Update");
        submitButton.setMaxSize(400, 50);
        submitButton.setMinSize(400, 50);
        //submitButton.setPadding(new Insets(0, 50, 0, 0));

        Button cancel = new Button("Back");
        cancel.setMaxSize(150, 40);
        cancel.setMinSize(150, 40);
        cancel.setTranslateX(250);
        cancel.setTranslateY(30);

        displayUserInfo.add(name, 0, 0);
        displayUserInfo.add(username, 0, 1);
        displayUserInfo.add(oldPassword, 0, 2);
        displayUserInfo.add(password, 0, 3);
        displayUserInfo.add(password2, 0, 4);
        displayUserInfo.add(submitButton, 0, 5);
        displayUserInfo.add(cancel, 0, 6);

        submitButton.setOnAction((event -> {
            if(!name.getText().trim().isEmpty() && name.getText().trim().matches("[A-Za-z0-9 ]+")) {
                UserInfo.updateName(name.getText().trim(), controller.userInfo[0]);
                controller.userInfo[1] = name.getText().trim();
                controller.leftSide.userButton.setText(controller.userInfo[1]);
                Popup.showConfirmation("Your Name has been updated");
                name.setText(controller.userInfo[1]);
            }
            if(!password.getText().trim().isEmpty() && password.getText().trim().matches("[A-Za-z0-9]+")) {
                if(password.getText().matches(password2.getText())) {
                    if (hidePass(oldPassword.getText()).matches(controller.userInfo[2])) {
                        UserInfo.updatePassword(hidePass(password.getText()), controller.userInfo[0]);
                        controller.userInfo[2] = hidePass(password.getText());
                        Popup.showConfirmation("Your password has been updated");
                    } else {
                        Popup.showError("Wrong current password!");
                    }
                } else {
                    Popup.showError("Mismatch Password! Or special characters used");
                }
            }

        }));

        cancel.setOnAction((event -> {
            if(Popup.yesNoCancelBox("You do not want to change your settings?")) {
                controller.leftSide.userButton.setDisable(false);
                controller.leftSide.startPlaying.setDisable(false);
                makeHomePage();
            }
        }));

        root.setCenter(displayUserInfo);
    }

    public void makeGameTypeSelection() {
        GridPane gameTypeDisplay = new GridPane();
        gameTypeDisplay.setMinSize(500, 400);
        gameTypeDisplay.setMaxSize(500, 400);
        gameTypeDisplay.setAlignment(Pos.CENTER);
        gameTypeDisplay.setHgap(10);
        gameTypeDisplay.setVgap(10);

        Button animals = unifyButton("Animals");

        Button games = unifyButton("Games");

        Button places = unifyButton("Places");

        Button plants = unifyButton("Plants");

        Button back = new Button("Go Back");
        back.setMaxSize(150, 40);
        back.setMinSize(150, 40);
        back.setTranslateX(250);
        back.setTranslateY(30);
        back.setFont(Font.font("Verdana", 14));

        gameTypeDisplay.add(animals, 0, 0);
        gameTypeDisplay.add(games, 0, 1);
        gameTypeDisplay.add(places, 0, 2);
        gameTypeDisplay.add(plants, 0, 3);
        gameTypeDisplay.add(back, 0, 4);

        animals.setOnAction((event -> {
            makeLevels(0);
            controller.bottomSide.makeLevelScreen();
            controller.topSide.makeLevelSelectionPage();
            controller.topSide.topHeadder.setText("Animals");
            controller.boardLogic.setWords(controller.user.gamePart[0].allWords);
        }));

        games.setOnAction((event -> {
            makeLevels(1);
            controller.bottomSide.makeLevelScreen();
            controller.topSide.makeLevelSelectionPage();
            controller.topSide.topHeadder.setText("Games");
            controller.boardLogic.setWords(controller.user.gamePart[1].allWords);
        }));

        places.setOnAction((event -> {
            makeLevels(2);
            controller.bottomSide.makeLevelScreen();
            controller.topSide.makeLevelSelectionPage();
            controller.topSide.topHeadder.setText("Places");
            controller.boardLogic.setWords(controller.user.gamePart[2].allWords);
        }));

        plants.setOnAction((event -> {
            makeLevels(3);
            controller.bottomSide.makeLevelScreen();
            controller.topSide.makeLevelSelectionPage();
            controller.topSide.topHeadder.setText("Plants");
            controller.boardLogic.setWords(controller.user.gamePart[3].allWords);
        }));

        back.setOnAction((event -> {
            controller.leftSide.startPlaying.setVisible(true);
            controller.leftSide.userButton.setVisible(true);
            controller.topSide.makeHomePage();
            controller.leftSide.homeButton.setVisible(false);
            makeHomePage();
        }));

        root.setCenter(gameTypeDisplay);
    }

    public Button unifyButton(String name) {
        Button button = new Button(name);
        button.setMaxSize(400, 50);
        button.setMinSize(400, 50);
        button.setFont(Font.font("Verdana", 14));

        return button;
    }

    public void makeLevels(int n) {
        Label[] labels = new Label[controller.user.gamePart[n].levels.length];
        gameComponents = new GameComponents(root, controller);
        for (int i = 0; i < labels.length; i++) {
            labels[i] = gameComponents.unifyLabel((char)(i + '1'), controller.user.gamePart[n].levels[i].unlocked);
        }
        gameComponents.setLetters(labels);
        gameComponents.remakeCenter();

        controller.levelSelection();
    }

    void makeGamePage(char[] chars) {
        controller.rightSide.replay.setOnAction((event -> {
            makeGamePage(chars);
            gameComponents.resetColor();
            gameComponents.remakeCenter();
            controller.rightSide.makeGameScreen();
        }));
        isPlaying = true;
        controller.leftSide.pause.setVisible(true);
        controller.leftSide.startPlaying.setOnAction((event -> {
            root.getCenter().setVisible(true);
            controller.leftSide.startPlaying.setVisible(false);
            controller.leftSide.pause.setVisible(true);
            //timer.cancel();
            doYou(1000, count);
        }));

        controller.leftSide.pause.setOnAction((event -> {
            root.getCenter().setVisible(false);
            controller.leftSide.startPlaying.setVisible(true);
            controller.leftSide.pause.setVisible(false);
            timer.cancel();
        }));

        Label[] labels = new Label[16];
        gameComponents = new GameComponents(root, controller);
        for (int i = 0; i < labels.length; i++) {
            labels[i] = gameComponents.unifyLabel(chars[i]);
        }
        gameComponents.setLetters(labels);
        gameComponents.remakeCenter();
        controller.rightSide.setTargerScore(controller.boardLogic.totalScore);
        gameComponents.gameReady();
        controller.bottomSide.back.setOnAction((event -> {
            if(isPlaying) {
                root.getCenter().setVisible(false);
                root.getLeft().setDisable(true);
                timer.cancel();
                isPlaying = !Popup.yesNoCancelBox("Are you sure you want to quit? You will lose it all");
            }
            root.getCenter().setVisible(true);
            root.getLeft().setDisable(false);
            controller.leftSide.startPlaying.setVisible(false);
            controller.leftSide.pause.setVisible(true);
            //timer.cancel();
            doYou(1000, count);

            if(!isPlaying) {
                String word = controller.topSide.topHeadder.getText().substring(0, 4);
                for (int i = 0; i < controller.user.gamePart.length; i++) {
                    if (controller.user.gamePart[i].partName.contains(word)) {
                        makeLevels(i);
                        controller.bottomSide.makeLevelScreen();
                        controller.topSide.topHeadder.setText(controller.user.gamePart[i].partName);
                    }
                }
                controller.rightSide.makeHomePage();
                try {
                    timer.cancel();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));
        doYou(1000, 60);


    }

    private void doSomething() {

    }

    public void doYou(int period, int count) {
        timer = new java.util.Timer();
        this.count = count;
        timer.schedule(new TimerTask() {
            public void run() {
                Platform.runLater(new Runnable() {
                    public void run() {
                        doCount();
                    }
                });
            }
        }, 0, period);
    }
    void doCount() {
        controller.rightSide.setTimer(count);
        count--;
        if(count < -1) {
            controller.leftSide.pause.setOnAction((event -> {

            }));
            controller.leftSide.startPlaying.setOnAction((event -> {

            }));
            controller.leftSide.startPlaying.setVisible(false);
            controller.leftSide.pause.setVisible(false);
            timer.cancel();
            Popup.showConfirmation("I am sorry. time ran out");
            root.getCenter().setDisable(true);
            for(int i = 0; i <controller.boardLogic.goodWords.size(); i++) {
                Label label = new Label(controller.boardLogic.goodWords.get(i));
                Label label1 = new Label("" + controller.boardLogic.points.get(i));
                label.setMaxSize(120, 40);
                label.setMinSize(120, 40);
                label1.setMaxSize(120, 40);
                label1.setMinSize(120, 40);

                controller.rightSide.wordsGuessed.getChildren().addAll(label, label1);
            }
        }
    }
}