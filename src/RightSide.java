import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordFramework
 * Created by manjur on 11/18/16 at 7:06 PM.
 */
public class RightSide {
    private BorderPane                  root                ;   // innitial instance of root
    GridPane                            rightFlowPane       ;   // Housing for rightside elenennt
    private Controller                  controller          ;   // universal controller
    FlowPane                            guessedLetters      ;   // leters that were guessed
    HBox[]                              letters             ;
    Text                                time                ;   // time remaining
    FlowPane                            wordsGuessed        ;   // Words guessed with score
    Text                                currentScore        ;   // current score
    Text                                targerScore         ;   // Target score
    Button                              replay              ;   // replay level


    public RightSide(BorderPane root) {
        this.root = root;
        makeHomePage();
    }

    void makeHomePage() {
        rightFlowPane = new GridPane();
        rightFlowPane.setMaxSize(330, 550);
        rightFlowPane.setMinSize(330, 550);
        rightFlowPane.setStyle("-fx-background-color: #E0EEEE");

        root.setRight(rightFlowPane);
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void makeGameScreen() {

        rightFlowPane = new GridPane();
        rightFlowPane.setStyle("-fx-background-color: #E0EEEE");
        rightFlowPane.setTranslateY(10);
        rightFlowPane.setMinSize(330, 550);
        rightFlowPane.setMaxSize(330, 550);
        rightFlowPane.setAlignment(Pos.CENTER);
        rightFlowPane.setHgap(10);
        rightFlowPane.setVgap(10);

        time = new Text();
        time.setText("60");
        time.setFont(Font.font("Verdana", 20));
        time.setFill(Color.FIREBRICK);

        makeLetters();
        guessedLetters = new FlowPane(letters);
        guessedLetters.setMaxSize(300, 172);
        guessedLetters.setMinSize(300, 172);
        guessedLetters.setVgap(6);
        guessedLetters.setHgap(6);

        wordsGuessed = new FlowPane();
        wordsGuessed.setStyle("-fx-background-color: #D3D3D3;");
        wordsGuessed.setMaxSize(300, 250);
        wordsGuessed.setMinSize(300, 250);
        wordsGuessed.setVgap(10);
        wordsGuessed.setHgap(10);

        currentScore = new Text();
        currentScore.setText("Current score: 0");


        targerScore = new Text();
        targerScore.setText("Target score: 0");

        rightFlowPane.add(time, 0, 0);
        rightFlowPane.add(guessedLetters, 0, 1);
        rightFlowPane.add(wordsGuessed, 0, 2);
        rightFlowPane.add(currentScore, 0, 3);
        rightFlowPane.add(targerScore, 0, 4);
        replay = new Button("Replay");
        rightFlowPane.add(replay, 0, 5);

        root.setRight(this.rightFlowPane);

    }

    void highLight(int i) {
        letters[i].setVisible(true);
    }

    void setTimer(int num) {
        time.setText(Integer.toString(num));
    }

    void setTargerScore(int num) {
        targerScore.setText("Target score: " + num);
    }

    void setCurrentScore(int num) {
        currentScore.setText("Current score: " + num);
    }

    void makeLetters() {
        letters = new HBox[26];
        for(int i = 0; i < 26; i++) {
            Label temp = new Label(Character.toString((char)(i + 'A')));
            temp.setFont(Font.font(16));
            letters[i] = new HBox(temp);
            letters[i].setAlignment(Pos.CENTER);
            letters[i].setId(Character.toString((char)(i + 'A')));
            letters[i].setMinSize(30, 30);
            letters[i].setMaxSize(30, 30);
            letters[i].setStyle("-fx-background-color: #0EBFE9;");
            letters[i].setVisible(false);
            //letters[i].setStyle("-fx-background-color: #7f7fff;");
        }
    }
    void updateLetters() {
        guessedLetters = new FlowPane(letters);
        rightFlowPane.add(guessedLetters, 0, 1);
    }
}