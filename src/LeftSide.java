import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordFramework
 * Created by manjur on 11/17/16 at 10:58 PM.
 */
public class LeftSide {
    private BorderPane          root                        ;   // main pane
    Button                      loginButton                 ;   // login button
    Button                      registerButton              ;   // register button
    Button                      logout                      ;   // logout
    Button                      pause                       ;   // pause button
    Button                      userButton                  ;   // User info
     String[]            userInfo                    ;   // userInfo
     User                user                        ;   // user
     Button              startPlaying                ;   // start playing
     FlowPane            leftPane                    ;   // leftside flow pane
     Controller          controller                  ;   // universal controller
    Button                      homeButton           ;   // always goes home
    Button                      helpButton          ;   //help button


    public LeftSide(BorderPane root) {
        this.root = root;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }
/*
    public void makeHomePage() {
        //user = new User(null);
        innitializeLoginButton();
        innitializeRegisterButton();

        leftPane = new FlowPane(Orientation.VERTICAL);
        leftPane.getChildren().addAll(loginButton, registerButton);
        leftPane.setMinSize(170, 550);
        leftPane.setMaxSize(170, 550);
        //leftPane.setTranslateY(100);
        leftPane.setStyle("-fx-background-color: #E0EEEE");

        root.setLeft(leftPane);
    }

    public void makeOtherPage() {
        FlowPane leftPane = new FlowPane(Orientation.VERTICAL);
        innitializeLogoutButton();
        innitializeUserButton();
        innitializeStartgameButton(leftPane);


        leftPane.getChildren().addAll(logout, userButton, startPlaying);
        leftPane.setMinSize(170, 550);
        leftPane.setMaxSize(170, 550);
        //leftPane.setTranslateY(100);
        leftPane.setStyle("-fx-background-color: #E0EEEE");

        root.setLeft(leftPane);
    }

    public void buildLevel(FlowPane left) {
        startPlaying.setVisible(false);
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "Animals",
                        "Plants",
                        "Places",
                        "Games"
                );
        final ComboBox<String> comboBox = new ComboBox(options);
        left.getChildren().add(comboBox);
        comboBox.setTranslateY(200);
        comboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String result = ((ComboBox<String>) e.getSource()).getValue();
                switch (result) {
                    case "Animals" :
                        CenterSide.makeGameComponents(user.getGamePart(0).getMaxLevels());
                        break;
                    case "Plants" :
                        CenterSide.makeGameComponents(user.getGamePart(1).getMaxLevels());
                        break;
                    case "Places" :
                        CenterSide.makeGameComponents(user.getGamePart(2).getMaxLevels());
                        break;
                    case "Games" :
                        CenterSide.makeGameComponents(user.getGamePart(3).getMaxLevels());
                        break;
                }
            }
        });
    }
*/
    public Button unifyButtonDesign(Button btn, double yTranslate) {
        btn.setMaxSize(150, 40);
        btn.setMinSize(150, 40);
        btn.setTranslateY(yTranslate);
        btn.setStyle("-fx-background-color: \n" +
                "        #3c7fb1,\n" +
                "        linear-gradient(#fafdfe, #e8f5fc),\n" +
                "        linear-gradient(#eaf6fd 0%, #d9f0fc 49%, #bee6fd 50%, #a7d9f5 100%);\n" +
                "    -fx-background-insets: 0,1,2;\n" +
                "    -fx-background-radius: 3,2,1;\n" +
                "    -fx-padding: 3 30 3 30;\n" +
                "    -fx-text-fill: black;\n" +
                "    -fx-font-size: 20px;");
        btn.setTranslateX(10);
        return btn;
    }

    public void makeLoginScreen() {
        helpButton = unifyButtonDesign(new Button("Help"), 350);
        loginButton = unifyButtonDesign(new Button("Login"), 230);
        registerButton = unifyButtonDesign(new Button("Register"), 280);
        leftPane = new FlowPane(Orientation.VERTICAL);
        leftPane.getChildren().addAll(loginButton, registerButton, helpButton);
        leftPane.setMinSize(170, 550);
        leftPane.setMaxSize(170, 550);
        //leftPane.setTranslateY(100);
        leftPane.setStyle("-fx-background-color: #E0EEEE");
        controller.innitializeLoginScreenButton();
        root.setLeft(leftPane);
    }

    public void makeHomeScreen() {
        //helpButton = unifyButtonDesign(new Button("Help"), 350);
        userButton = unifyButtonDesign(new Button(controller.userInfo[1]), 10);
        startPlaying = unifyButtonDesign(new Button("Play"), 200);
        homeButton = unifyButtonDesign(new Button("Home"), 200); // hidden when at home screen
        pause = unifyButtonDesign(new Button("Pause"), 200); // hidden all the time except when playing
        logout = unifyButtonDesign(new Button("Logout"), 400);
        leftPane = new FlowPane(Orientation.VERTICAL);
        leftPane.getChildren().addAll(userButton, startPlaying, logout, homeButton, pause, helpButton);
        leftPane.setMinSize(170, 550);
        leftPane.setMaxSize(170, 550);
        leftPane.setStyle("-fx-background-color: #E0EEEE");
        controller.innitializeHomeScreenButton();

        homeButton.setVisible(false);
        pause.setVisible(false);

        root.setLeft(leftPane);
    }
}