import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Created in : PACKAGE_NAME
 * Project name: BuzzWordFramework
 * Created by manjur on 11/17/16 at 10:38 PM.
 */
public class main extends Application {
    private static BorderPane              root                ;   // This is the very first layer
    private static LeftSide                leftSide            ;   // Left side of the screen
    private static TopSide                 topSide             ;   // Top side of the screen
    private static BottomSide              bottomSide          ;   // Bottom side of the screen
    private static RightSide               rightSide           ;   // Right side of the screen
    private static CenterSide              centerSide          ;   // Center side of the screen
    private static Controller              controller          ;   // controller

    public static void makeWindow() {
        UserInfo userInfo = new UserInfo();
        root = new BorderPane();
        root.setMinSize(1000, 750);
        root.setMaxSize(1000, 750);
        leftSide = new LeftSide(root);
        topSide = new TopSide(root);
        bottomSide = new BottomSide(root);
        rightSide = new RightSide(root);
        centerSide = new CenterSide(root);
        controller = new Controller(rightSide, leftSide, topSide, bottomSide, centerSide);

    }
    @Override
    public void start(Stage stage) throws Exception {

        Platform.setImplicitExit(false);
        makeWindow();
        stage.setTitle("¡¡BuzzWord!!");
        stage.setWidth(1000);
        stage.setHeight(790);
        stage.setScene(new Scene(root, 1000, 790));
        stage.setResizable(false);
        stage.show();
        stage.setOnCloseRequest((event -> {
            try {

                if (centerSide.isPlaying) {
                    root.getCenter().setVisible(false);
                    root.getLeft().setDisable(true);
                    centerSide.timer.cancel();
                    centerSide.isPlaying = !Popup.yesNoCancelBox("Are you sure you want to quit? You will lose it all");
                }
                root.getCenter().setVisible(true);
                root.getLeft().setDisable(false);
                leftSide.startPlaying.setVisible(false);
                leftSide.pause.setVisible(true);
                //timer.cancel();
                centerSide.doYou(100, centerSide.count);

                if (centerSide.isPlaying) {
                    event.consume();
                } else {
                    UserInfo.saveUserInfo();
                    if (controller.userLoggedIn) {
                        controller.user.saveUserInfo();
                    }
                    stage.close();
                }

            }catch (Exception e) {
                stage.close();
            }

            //UserInfo.saveUserInfo();

            //event.consume();
        }));
        /*stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent windowEvent) {

            }
        });*/
        }

    public static void main(String[] args) {
        launch(args);
    }
}